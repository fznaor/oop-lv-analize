﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Z2
{
    public partial class Form1 : Form
    {
        List<string> Rijeci = new List<string>();
        Igra i;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            buttonNovaIgra.Visible = false;
            buttonIzlaz.Visible = false;
            using (System.IO.StreamReader reader = new System.IO.StreamReader("ListaRijeci.txt"))
            {
                string line;
                line = reader.ReadLine();
                string[] parts = line.Split(',');
                foreach (string s in parts)
                    Rijeci.Add(s);
            }
            Random rnd = new Random();
            int n = rnd.Next(0, Rijeci.Count-1);
            string clue = Rijeci[n];
            i = new Igra(clue);
            i.Ispis();
            labelWord.Text = i.forLabel;
            labelLifeCount.Text = i.brojZivota.ToString();
        }

        public class Igra
        {
            public int brojZivota { get; private set; }
            public string rijec { get; private set; }
            public List<char> pokusaji { get; set; }
            private string pogodjenaSlova=String.Empty;
            public int brojPogodjenihSlova { get; private set; }
            public string forLabel { get; private set; }
            public Igra(string r)
            {
                rijec = r;
                brojZivota = 6;
                pokusaji = new List<char>();
                brojPogodjenihSlova = 0;
            }
            public bool IsFound(char c)
            {
                foreach(char d in pogodjenaSlova)
                {
                    if (d == c) return true;
                }
                return false;
            }
            public void Ispis()
            {
                forLabel = String.Empty;
                foreach(char c in rijec)
                {
                    if (c == ' ') forLabel += c;
                    else if (IsFound(c)) forLabel += c;
                    else forLabel += '_';
                    forLabel += ' ';
                }
            }
            public void Guess(char c)
            {
                c = char.ToUpper(c);
                pokusaji.Add(c);
                foreach(char d in rijec)
                {
                    if (d == c)
                    {
                        brojPogodjenihSlova++;
                        pogodjenaSlova += c;
                    }
                }
                if (!IsFound(c)) brojZivota--;
            }
            public int spaceCount()
            {
                int space = 0;
                foreach (char c in rijec)
                    if (c == ' ') space++;
                return space;
            }
            public bool AlreadyTried(char c)
            {
                foreach (char d in pokusaji)
                    if (c == d) return true;
                return false;
            }
        }

        private void buttonPogadjaj_Click(object sender, EventArgs e)
        {
            if (textBoxSlovo.Text == String.Empty) MessageBox.Show("Niste unijeli slovo!", "Pogreška");
            else if (i.AlreadyTried(char.ToUpper(textBoxSlovo.Text[0]))) { MessageBox.Show("Već ste pogađali to slovo, unesite drugo!", "Pogreška"); textBoxSlovo.Text = String.Empty; }
            else
            {
                i.Guess(textBoxSlovo.Text[0]);
                i.Ispis();
                labelWord.Text = i.forLabel;
                labelLifeCount.Text = i.brojZivota.ToString();
                textBoxSlovo.Text = String.Empty;
                if (i.brojZivota == 0)
                {
                    MessageBox.Show("Izgubili ste!", "Game over");
                    labelWord.Text = i.rijec;
                    labelUnos.Visible = false;
                    textBoxSlovo.Visible = false;
                    buttonPogadjaj.Visible = false;
                    buttonIzlaz.Visible = true;
                    buttonNovaIgra.Visible = true;
                }
                else if (i.brojPogodjenihSlova + i.spaceCount() == i.rijec.Length)
                {
                    MessageBox.Show("Pobijedili ste!", "Pobjeda");
                    buttonPogadjaj.Visible = false;
                    labelUnos.Visible = false;
                    textBoxSlovo.Visible = false;
                    buttonIzlaz.Visible = true;
                    buttonNovaIgra.Visible = true;
                }
            }
        }

        private void buttonIzlaz_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void buttonNovaIgra_Click(object sender, EventArgs e)
        {
            buttonPogadjaj.Visible = true;
            labelUnos.Visible = true;
            textBoxSlovo.Visible = true;
            buttonIzlaz.Visible = false;
            buttonNovaIgra.Visible = false;
            Random rnd = new Random();
            int n = rnd.Next(0, Rijeci.Count-1);
            string clue = Rijeci[n];
            i = new Igra(clue);
            i.Ispis();
            labelWord.Text = i.forLabel;
            labelLifeCount.Text = i.brojZivota.ToString();
        }
    }
}
