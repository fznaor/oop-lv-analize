﻿namespace Z2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelWord = new System.Windows.Forms.Label();
            this.labelPP = new System.Windows.Forms.Label();
            this.labelLifeCount = new System.Windows.Forms.Label();
            this.labelUnos = new System.Windows.Forms.Label();
            this.textBoxSlovo = new System.Windows.Forms.TextBox();
            this.buttonPogadjaj = new System.Windows.Forms.Button();
            this.buttonNovaIgra = new System.Windows.Forms.Button();
            this.buttonIzlaz = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // labelWord
            // 
            this.labelWord.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelWord.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelWord.Location = new System.Drawing.Point(0, 0);
            this.labelWord.Name = "labelWord";
            this.labelWord.Size = new System.Drawing.Size(890, 203);
            this.labelWord.TabIndex = 0;
            this.labelWord.Text = "label1";
            this.labelWord.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // labelPP
            // 
            this.labelPP.AutoSize = true;
            this.labelPP.Location = new System.Drawing.Point(406, 58);
            this.labelPP.Name = "labelPP";
            this.labelPP.Size = new System.Drawing.Size(36, 13);
            this.labelPP.TabIndex = 1;
            this.labelPP.Text = "Životi:";
            // 
            // labelLifeCount
            // 
            this.labelLifeCount.AutoSize = true;
            this.labelLifeCount.Location = new System.Drawing.Point(439, 58);
            this.labelLifeCount.Name = "labelLifeCount";
            this.labelLifeCount.Size = new System.Drawing.Size(35, 13);
            this.labelLifeCount.TabIndex = 2;
            this.labelLifeCount.Text = "label1";
            // 
            // labelUnos
            // 
            this.labelUnos.AutoSize = true;
            this.labelUnos.Location = new System.Drawing.Point(372, 94);
            this.labelUnos.Name = "labelUnos";
            this.labelUnos.Size = new System.Drawing.Size(132, 13);
            this.labelUnos.TabIndex = 3;
            this.labelUnos.Text = "Slovo koje želite pogađati:";
            // 
            // textBoxSlovo
            // 
            this.textBoxSlovo.Location = new System.Drawing.Point(510, 91);
            this.textBoxSlovo.MaxLength = 1;
            this.textBoxSlovo.Name = "textBoxSlovo";
            this.textBoxSlovo.Size = new System.Drawing.Size(17, 20);
            this.textBoxSlovo.TabIndex = 4;
            // 
            // buttonPogadjaj
            // 
            this.buttonPogadjaj.Location = new System.Drawing.Point(409, 130);
            this.buttonPogadjaj.Name = "buttonPogadjaj";
            this.buttonPogadjaj.Size = new System.Drawing.Size(75, 23);
            this.buttonPogadjaj.TabIndex = 5;
            this.buttonPogadjaj.Text = "Pogađaj";
            this.buttonPogadjaj.UseVisualStyleBackColor = true;
            this.buttonPogadjaj.Click += new System.EventHandler(this.buttonPogadjaj_Click);
            // 
            // buttonNovaIgra
            // 
            this.buttonNovaIgra.Location = new System.Drawing.Point(355, 168);
            this.buttonNovaIgra.Name = "buttonNovaIgra";
            this.buttonNovaIgra.Size = new System.Drawing.Size(75, 23);
            this.buttonNovaIgra.TabIndex = 6;
            this.buttonNovaIgra.Text = "Nova igra";
            this.buttonNovaIgra.UseVisualStyleBackColor = true;
            this.buttonNovaIgra.Click += new System.EventHandler(this.buttonNovaIgra_Click);
            // 
            // buttonIzlaz
            // 
            this.buttonIzlaz.Location = new System.Drawing.Point(442, 168);
            this.buttonIzlaz.Name = "buttonIzlaz";
            this.buttonIzlaz.Size = new System.Drawing.Size(75, 23);
            this.buttonIzlaz.TabIndex = 7;
            this.buttonIzlaz.Text = "Izlaz";
            this.buttonIzlaz.UseVisualStyleBackColor = true;
            this.buttonIzlaz.Click += new System.EventHandler(this.buttonIzlaz_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(890, 203);
            this.Controls.Add(this.buttonIzlaz);
            this.Controls.Add(this.buttonNovaIgra);
            this.Controls.Add(this.buttonPogadjaj);
            this.Controls.Add(this.textBoxSlovo);
            this.Controls.Add(this.labelUnos);
            this.Controls.Add(this.labelLifeCount);
            this.Controls.Add(this.labelPP);
            this.Controls.Add(this.labelWord);
            this.Name = "Form1";
            this.Text = "Vješala";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelWord;
        private System.Windows.Forms.Label labelPP;
        private System.Windows.Forms.Label labelLifeCount;
        private System.Windows.Forms.Label labelUnos;
        private System.Windows.Forms.TextBox textBoxSlovo;
        private System.Windows.Forms.Button buttonPogadjaj;
        private System.Windows.Forms.Button buttonNovaIgra;
        private System.Windows.Forms.Button buttonIzlaz;
    }
}

