﻿namespace Z1
{
    partial class ZnanstveniKalk
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelPrvi = new System.Windows.Forms.Label();
            this.labelDrugi = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.labelOper = new System.Windows.Forms.Label();
            this.comboBoxOperacije = new System.Windows.Forms.ComboBox();
            this.buttonIzracunaj = new System.Windows.Forms.Button();
            this.labelRezTxt = new System.Windows.Forms.Label();
            this.labelRez = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // labelPrvi
            // 
            this.labelPrvi.AutoSize = true;
            this.labelPrvi.Location = new System.Drawing.Point(24, 52);
            this.labelPrvi.Name = "labelPrvi";
            this.labelPrvi.Size = new System.Drawing.Size(70, 13);
            this.labelPrvi.TabIndex = 0;
            this.labelPrvi.Text = "Prvi operand:";
            // 
            // labelDrugi
            // 
            this.labelDrugi.AutoSize = true;
            this.labelDrugi.Location = new System.Drawing.Point(17, 86);
            this.labelDrugi.Name = "labelDrugi";
            this.labelDrugi.Size = new System.Drawing.Size(77, 13);
            this.labelDrugi.TabIndex = 1;
            this.labelDrugi.Text = "Drugi operand:";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(111, 49);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(167, 20);
            this.textBox1.TabIndex = 2;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(111, 83);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(167, 20);
            this.textBox2.TabIndex = 3;
            // 
            // labelOper
            // 
            this.labelOper.AutoSize = true;
            this.labelOper.Location = new System.Drawing.Point(171, 123);
            this.labelOper.Name = "labelOper";
            this.labelOper.Size = new System.Drawing.Size(55, 13);
            this.labelOper.TabIndex = 4;
            this.labelOper.Text = "Operacija:";
            // 
            // comboBoxOperacije
            // 
            this.comboBoxOperacije.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxOperacije.FormattingEnabled = true;
            this.comboBoxOperacije.Items.AddRange(new object[] {
            "+",
            "-",
            "*",
            "/",
            "√",
            "x^2",
            "sin",
            "cos",
            "tg"});
            this.comboBoxOperacije.Location = new System.Drawing.Point(232, 120);
            this.comboBoxOperacije.Name = "comboBoxOperacije";
            this.comboBoxOperacije.Size = new System.Drawing.Size(46, 21);
            this.comboBoxOperacije.TabIndex = 5;
            this.comboBoxOperacije.SelectedIndexChanged += new System.EventHandler(this.comboBoxOperacije_SelectedIndexChanged);
            // 
            // buttonIzracunaj
            // 
            this.buttonIzracunaj.Location = new System.Drawing.Point(203, 176);
            this.buttonIzracunaj.Name = "buttonIzracunaj";
            this.buttonIzracunaj.Size = new System.Drawing.Size(75, 23);
            this.buttonIzracunaj.TabIndex = 6;
            this.buttonIzracunaj.Text = "Izračunaj";
            this.buttonIzracunaj.UseVisualStyleBackColor = true;
            this.buttonIzracunaj.Click += new System.EventHandler(this.buttonIzracunaj_Click);
            // 
            // labelRezTxt
            // 
            this.labelRezTxt.AutoSize = true;
            this.labelRezTxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelRezTxt.Location = new System.Drawing.Point(24, 218);
            this.labelRezTxt.Name = "labelRezTxt";
            this.labelRezTxt.Size = new System.Drawing.Size(58, 13);
            this.labelRezTxt.TabIndex = 7;
            this.labelRezTxt.Text = "Rezultat:";
            // 
            // labelRez
            // 
            this.labelRez.AutoSize = true;
            this.labelRez.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelRez.Location = new System.Drawing.Point(88, 218);
            this.labelRez.Name = "labelRez";
            this.labelRez.Size = new System.Drawing.Size(41, 13);
            this.labelRez.TabIndex = 8;
            this.labelRez.Text = "label2";
            // 
            // ZnanstveniKalk
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(321, 259);
            this.Controls.Add(this.labelRez);
            this.Controls.Add(this.labelRezTxt);
            this.Controls.Add(this.buttonIzracunaj);
            this.Controls.Add(this.comboBoxOperacije);
            this.Controls.Add(this.labelOper);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.labelDrugi);
            this.Controls.Add(this.labelPrvi);
            this.Name = "ZnanstveniKalk";
            this.Text = "Znanstveni Kalkulator";
            this.Load += new System.EventHandler(this.ZnanstveniKalk_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelPrvi;
        private System.Windows.Forms.Label labelDrugi;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label labelOper;
        private System.Windows.Forms.ComboBox comboBoxOperacije;
        private System.Windows.Forms.Button buttonIzracunaj;
        private System.Windows.Forms.Label labelRezTxt;
        private System.Windows.Forms.Label labelRez;
    }
}

