﻿namespace Z1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox00 = new System.Windows.Forms.PictureBox();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.pictureBox22 = new System.Windows.Forms.PictureBox();
            this.pictureBox21 = new System.Windows.Forms.PictureBox();
            this.pictureBox20 = new System.Windows.Forms.PictureBox();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.pictureBox02 = new System.Windows.Forms.PictureBox();
            this.pictureBox01 = new System.Windows.Forms.PictureBox();
            this.labelp1 = new System.Windows.Forms.Label();
            this.labelp2 = new System.Windows.Forms.Label();
            this.labelp1score = new System.Windows.Forms.Label();
            this.labelp2score = new System.Windows.Forms.Label();
            this.labelturn = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.buttonStart = new System.Windows.Forms.Button();
            this.buttonNovaIgra = new System.Windows.Forms.Button();
            this.buttonIzlaz = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox00)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox01)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::Z1.Properties.Resources.tictactoe;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(6, 5);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(475, 471);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // pictureBox00
            // 
            this.pictureBox00.Location = new System.Drawing.Point(18, 16);
            this.pictureBox00.Name = "pictureBox00";
            this.pictureBox00.Size = new System.Drawing.Size(127, 127);
            this.pictureBox00.TabIndex = 1;
            this.pictureBox00.TabStop = false;
            this.pictureBox00.Visible = false;
            this.pictureBox00.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pb00_mup);
            // 
            // pictureBox12
            // 
            this.pictureBox12.Location = new System.Drawing.Point(338, 173);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(127, 127);
            this.pictureBox12.TabIndex = 2;
            this.pictureBox12.TabStop = false;
            this.pictureBox12.Visible = false;
            this.pictureBox12.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pb12_mup);
            // 
            // pictureBox22
            // 
            this.pictureBox22.Location = new System.Drawing.Point(338, 334);
            this.pictureBox22.Name = "pictureBox22";
            this.pictureBox22.Size = new System.Drawing.Size(127, 127);
            this.pictureBox22.TabIndex = 3;
            this.pictureBox22.TabStop = false;
            this.pictureBox22.Visible = false;
            this.pictureBox22.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pb22_mup);
            // 
            // pictureBox21
            // 
            this.pictureBox21.Location = new System.Drawing.Point(175, 334);
            this.pictureBox21.Name = "pictureBox21";
            this.pictureBox21.Size = new System.Drawing.Size(127, 127);
            this.pictureBox21.TabIndex = 4;
            this.pictureBox21.TabStop = false;
            this.pictureBox21.Visible = false;
            this.pictureBox21.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pb21_mup);
            // 
            // pictureBox20
            // 
            this.pictureBox20.Location = new System.Drawing.Point(18, 334);
            this.pictureBox20.Name = "pictureBox20";
            this.pictureBox20.Size = new System.Drawing.Size(127, 127);
            this.pictureBox20.TabIndex = 5;
            this.pictureBox20.TabStop = false;
            this.pictureBox20.Visible = false;
            this.pictureBox20.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pb20_mup);
            // 
            // pictureBox11
            // 
            this.pictureBox11.Location = new System.Drawing.Point(175, 173);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(127, 127);
            this.pictureBox11.TabIndex = 6;
            this.pictureBox11.TabStop = false;
            this.pictureBox11.Visible = false;
            this.pictureBox11.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pb11_mup);
            // 
            // pictureBox10
            // 
            this.pictureBox10.Location = new System.Drawing.Point(18, 173);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(127, 127);
            this.pictureBox10.TabIndex = 7;
            this.pictureBox10.TabStop = false;
            this.pictureBox10.Visible = false;
            this.pictureBox10.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pb10_mup);
            // 
            // pictureBox02
            // 
            this.pictureBox02.Location = new System.Drawing.Point(338, 16);
            this.pictureBox02.Name = "pictureBox02";
            this.pictureBox02.Size = new System.Drawing.Size(127, 127);
            this.pictureBox02.TabIndex = 8;
            this.pictureBox02.TabStop = false;
            this.pictureBox02.Visible = false;
            this.pictureBox02.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pb02_mup);
            // 
            // pictureBox01
            // 
            this.pictureBox01.Location = new System.Drawing.Point(175, 16);
            this.pictureBox01.Name = "pictureBox01";
            this.pictureBox01.Size = new System.Drawing.Size(127, 127);
            this.pictureBox01.TabIndex = 9;
            this.pictureBox01.TabStop = false;
            this.pictureBox01.Visible = false;
            this.pictureBox01.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pb01_mup);
            // 
            // labelp1
            // 
            this.labelp1.AutoSize = true;
            this.labelp1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelp1.Location = new System.Drawing.Point(533, 216);
            this.labelp1.Name = "labelp1";
            this.labelp1.Size = new System.Drawing.Size(53, 13);
            this.labelp1.TabIndex = 10;
            this.labelp1.Text = "Player1:";
            this.labelp1.Visible = false;
            // 
            // labelp2
            // 
            this.labelp2.AutoSize = true;
            this.labelp2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelp2.Location = new System.Drawing.Point(533, 259);
            this.labelp2.Name = "labelp2";
            this.labelp2.Size = new System.Drawing.Size(53, 13);
            this.labelp2.TabIndex = 11;
            this.labelp2.Text = "Player2:";
            this.labelp2.Visible = false;
            // 
            // labelp1score
            // 
            this.labelp1score.AutoSize = true;
            this.labelp1score.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelp1score.Location = new System.Drawing.Point(729, 216);
            this.labelp1score.Name = "labelp1score";
            this.labelp1score.Size = new System.Drawing.Size(41, 13);
            this.labelp1score.TabIndex = 12;
            this.labelp1score.Text = "label3";
            this.labelp1score.Visible = false;
            // 
            // labelp2score
            // 
            this.labelp2score.AutoSize = true;
            this.labelp2score.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelp2score.Location = new System.Drawing.Point(729, 259);
            this.labelp2score.Name = "labelp2score";
            this.labelp2score.Size = new System.Drawing.Size(41, 13);
            this.labelp2score.TabIndex = 13;
            this.labelp2score.Text = "label4";
            this.labelp2score.Visible = false;
            // 
            // labelturn
            // 
            this.labelturn.AutoSize = true;
            this.labelturn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelturn.Location = new System.Drawing.Point(533, 160);
            this.labelturn.Name = "labelturn";
            this.labelturn.Size = new System.Drawing.Size(41, 13);
            this.labelturn.TabIndex = 14;
            this.labelturn.Text = "label5";
            this.labelturn.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(533, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(106, 13);
            this.label1.TabIndex = 15;
            this.label1.Text = "Ime prvog igrača:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(533, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(113, 13);
            this.label2.TabIndex = 16;
            this.label2.Text = "Ime drugog igrača:";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(645, 22);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(190, 20);
            this.textBox1.TabIndex = 17;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(645, 57);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(190, 20);
            this.textBox2.TabIndex = 18;
            // 
            // buttonStart
            // 
            this.buttonStart.Location = new System.Drawing.Point(760, 99);
            this.buttonStart.Name = "buttonStart";
            this.buttonStart.Size = new System.Drawing.Size(75, 23);
            this.buttonStart.TabIndex = 19;
            this.buttonStart.Text = "Započni igru";
            this.buttonStart.UseVisualStyleBackColor = true;
            this.buttonStart.Click += new System.EventHandler(this.buttonStart_Click);
            // 
            // buttonNovaIgra
            // 
            this.buttonNovaIgra.Location = new System.Drawing.Point(536, 415);
            this.buttonNovaIgra.Name = "buttonNovaIgra";
            this.buttonNovaIgra.Size = new System.Drawing.Size(75, 23);
            this.buttonNovaIgra.TabIndex = 20;
            this.buttonNovaIgra.Text = "Nova igra";
            this.buttonNovaIgra.UseVisualStyleBackColor = true;
            this.buttonNovaIgra.Visible = false;
            this.buttonNovaIgra.Click += new System.EventHandler(this.buttonNovaIgra_Click);
            // 
            // buttonIzlaz
            // 
            this.buttonIzlaz.Location = new System.Drawing.Point(722, 415);
            this.buttonIzlaz.Name = "buttonIzlaz";
            this.buttonIzlaz.Size = new System.Drawing.Size(75, 23);
            this.buttonIzlaz.TabIndex = 21;
            this.buttonIzlaz.Text = "Izlaz";
            this.buttonIzlaz.UseVisualStyleBackColor = true;
            this.buttonIzlaz.Visible = false;
            this.buttonIzlaz.Click += new System.EventHandler(this.buttonIzlaz_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(859, 547);
            this.Controls.Add(this.buttonIzlaz);
            this.Controls.Add(this.buttonNovaIgra);
            this.Controls.Add(this.buttonStart);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.labelturn);
            this.Controls.Add(this.labelp2score);
            this.Controls.Add(this.labelp1score);
            this.Controls.Add(this.labelp2);
            this.Controls.Add(this.labelp1);
            this.Controls.Add(this.pictureBox01);
            this.Controls.Add(this.pictureBox02);
            this.Controls.Add(this.pictureBox10);
            this.Controls.Add(this.pictureBox11);
            this.Controls.Add(this.pictureBox20);
            this.Controls.Add(this.pictureBox21);
            this.Controls.Add(this.pictureBox22);
            this.Controls.Add(this.pictureBox12);
            this.Controls.Add(this.pictureBox00);
            this.Controls.Add(this.pictureBox1);
            this.Name = "Form1";
            this.Text = "Križić kružić";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox00)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox01)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox00;
        private System.Windows.Forms.PictureBox pictureBox12;
        private System.Windows.Forms.PictureBox pictureBox22;
        private System.Windows.Forms.PictureBox pictureBox21;
        private System.Windows.Forms.PictureBox pictureBox20;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.PictureBox pictureBox02;
        private System.Windows.Forms.PictureBox pictureBox01;
        private System.Windows.Forms.Label labelp1;
        private System.Windows.Forms.Label labelp2;
        private System.Windows.Forms.Label labelp1score;
        private System.Windows.Forms.Label labelp2score;
        private System.Windows.Forms.Label labelturn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Button buttonStart;
        private System.Windows.Forms.Button buttonNovaIgra;
        private System.Windows.Forms.Button buttonIzlaz;
    }
}

