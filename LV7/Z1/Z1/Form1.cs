﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Z1
{
    public partial class Form1 : Form
    {
        Graphics g1, g00, g01, g02, g10, g11, g12, g20 ,g21, g22;
        Pen pen = new Pen(Color.Blue, 10);
        Igra i;
        int pobjedePrvi, pobjedeDrugi;

        public Form1()
        {
            InitializeComponent();
            g1 = pictureBox1.CreateGraphics();
            g00 = pictureBox00.CreateGraphics();
            g01 = pictureBox01.CreateGraphics();
            g02 = pictureBox02.CreateGraphics();
            g10 = pictureBox10.CreateGraphics();
            g11 = pictureBox11.CreateGraphics();
            g12 = pictureBox12.CreateGraphics();
            g20 = pictureBox20.CreateGraphics();
            g21 = pictureBox21.CreateGraphics();
            g22 = pictureBox22.CreateGraphics();
        }

        class Igra
        {
            public string p1 { get; set; }
            public string p2 { get; set; }
            public int naPotezu { get; set; }
            int[][] polja;
            public Igra(string pr,string dr)
            {
                p1 = pr;
                p2 = dr;
                naPotezu = 1;
                polja = new int[3][];
                for (int i = 0; i < 3; i++)
                    polja[i] = new int[3];
                for (int i = 0; i < 3; i++)
                    for (int j = 0; j < 3; j++)
                        polja[i][j] = 0;
            }
            public bool pobjeda()
            {
                int a00 = polja[0][0];
                int a01 = polja[0][1];
                int a02 = polja[0][2];
                int a10 = polja[1][0];
                int a11 = polja[1][1];
                int a12 = polja[1][2];
                int a20 = polja[2][0];
                int a21 = polja[2][1];
                int a22 = polja[2][2];
                return ((a00 == a01 && a00 == a02 && a00 != 0) || (a10 == a11 && a10 == a12 && a10 != 0) || (a20 == a21 && a20 == a22 && a20 != 0)
                      || (a00 == a10 && a00 == a20 && a00 != 0) || (a01 == a11 && a01 == a21 && a01 != 0) || (a02 == a12 && a02 == a22 && a02 != 0)
                      || (a00 == a11 && a11 == a22 && a00 != 0) || (a02 == a11 && a02 == a20 && a02 != 0));
            }
            public bool nerijeseno()
            {
                for (int i = 0; i < 3; i++)
                    for (int j = 0; j < 3; j++)
                        if (polja[i][j] == 0) return false;
                return true;
            }
            public bool vazeciPotez(int i,int j)
            {
                return (polja[i][j]==0);
            }
            public void potez(int i,int j)
            {
                if (naPotezu == 1)
                {
                    polja[i][j] = 1;
                    naPotezu = 2;
                }
                else
                {
                    polja[i][j] = 2;
                    naPotezu = 1;
                }
            }
        }

        private void buttonStart_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == String.Empty || textBox2.Text == String.Empty) MessageBox.Show("Jedno od imena nije unešeno", "Greška!");
            else
            {
                i = new Igra(textBox1.Text, textBox2.Text);
                labelp1.Text = textBox1.Text;
                labelp2.Text = textBox2.Text;
                pobjedePrvi = 0;
                pobjedeDrugi = 0;
                labelp1score.Text = pobjedePrvi.ToString();
                labelp2score.Text = pobjedeDrugi.ToString();
                if (i.naPotezu == 1)
                    labelturn.Text = "Na potezu je " + i.p1;
                else labelturn.Text = "Na potezu je " + i.p2;
                pictureBox00.Visible = true;
                pictureBox01.Visible = true;
                pictureBox02.Visible = true;
                pictureBox10.Visible = true;
                pictureBox11.Visible = true;
                pictureBox12.Visible = true;
                pictureBox20.Visible = true;
                pictureBox21.Visible = true;
                pictureBox22.Visible = true;
                labelturn.Visible = true;
                labelp1.Visible = true;
                labelp2.Visible = true;
                labelp1score.Visible = true;
                labelp2score.Visible = true;
                label1.Visible = false;
                label2.Visible = false;
                textBox1.Visible = false;
                textBox2.Visible = false;
                buttonStart.Visible = false;
            }
        }

        public void pobjedaIliNerijeseno()
        {
            if (i.pobjeda())
            {
                string s;
                if (i.naPotezu == 2)
                {
                    s = "Pobjednik je " + i.p1;
                    pobjedePrvi++;
                    labelp1score.Text = pobjedePrvi.ToString();
                }
                else {
                    s = "Pobjednik je " + i.p2;
                    pobjedeDrugi++;
                    labelp2score.Text = pobjedeDrugi.ToString();
                }
                MessageBox.Show(s, "Pobjeda!");
            }
            else if (i.nerijeseno())
            {
                MessageBox.Show("Neriješeno!", "Kraj");
            }
            if(i.pobjeda() || i.nerijeseno())
            {
                pictureBox00.Visible = false;
                pictureBox01.Visible = false;
                pictureBox02.Visible = false;
                pictureBox10.Visible = false;
                pictureBox11.Visible = false;
                pictureBox12.Visible = false;
                pictureBox20.Visible = false;
                pictureBox21.Visible = false;
                pictureBox22.Visible = false;
                labelturn.Visible = false;
                buttonNovaIgra.Visible = true;
                buttonIzlaz.Visible = true;
            }
        }

        private void buttonIzlaz_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void buttonNovaIgra_Click(object sender, EventArgs e)
        {
            i = new Igra(textBox1.Text, textBox2.Text);
            buttonIzlaz.Visible = false;
            buttonNovaIgra.Visible = false;
            pictureBox00.Visible = true;
            pictureBox01.Visible = true;
            pictureBox02.Visible = true;
            pictureBox10.Visible = true;
            pictureBox11.Visible = true;
            pictureBox12.Visible = true;
            pictureBox20.Visible = true;
            pictureBox21.Visible = true;
            pictureBox22.Visible = true;
            labelturn.Visible = true;
            if (i.naPotezu == 1)
                labelturn.Text = "Na potezu je " + i.p1;
            else labelturn.Text = "Na potezu je " + i.p2;
        }

        void draw(Graphics gr)
        {
            if (i.naPotezu == 1)
            {
                labelturn.Text = "Na potezu je " + i.p1;
                gr.DrawEllipse(pen, 127 / 4, 127 / 4, 60, 60);
            }
            else
            {
                labelturn.Text = "Na potezu je " + i.p2;
                gr.DrawLine(pen, 127 / 2 - 30, 127 / 2 - 30, 127 / 2 + 30, 127 / 2 + 30);
                gr.DrawLine(pen, 127 / 2 + 30, 127 / 2 - 30, 127 / 2 - 30, 127 / 2 + 30);
            }
        }

        private void pb12_mup(object sender, MouseEventArgs e)
        {
            if (!i.vazeciPotez(1, 2)) MessageBox.Show("To polje je već iskorišteno!", "Greška!");
            else
            {
                i.potez(1, 2);
                if (i.naPotezu == 1)
                {
                    labelturn.Text = "Na potezu je " + i.p1;
                    g12.DrawEllipse(pen, 127 / 4, 127 / 4, 60, 60);
                }
                else
                {
                    labelturn.Text = "Na potezu je " + i.p2;
                    g12.DrawLine(pen, 127 / 2 - 30, 127 / 2 - 30, 127 / 2 + 30, 127 / 2 + 30);
                    g12.DrawLine(pen, 127 / 2 + 30, 127 / 2 - 30, 127 / 2 - 30, 127 / 2 + 30);
                }
                pobjedaIliNerijeseno();
            }
        }

        private void pb20_mup(object sender, MouseEventArgs e)
        {
            if (!i.vazeciPotez(2, 0)) MessageBox.Show("To polje je već iskorišteno!", "Greška!");
            else
            {
                i.potez(2, 0);
                draw(g20);
                pobjedaIliNerijeseno();
            }
        }

        private void pb21_mup(object sender, MouseEventArgs e)
        {
            if (!i.vazeciPotez(2, 1)) MessageBox.Show("To polje je već iskorišteno!", "Greška!");
            else
            {
                i.potez(2, 1);
                draw(g21);
                pobjedaIliNerijeseno();
            }
        }

        private void pb22_mup(object sender, MouseEventArgs e)
        {
            if (!i.vazeciPotez(2, 2)) MessageBox.Show("To polje je već iskorišteno!", "Greška!");
            else
            {
                i.potez(2, 2);
                draw(g22);
                pobjedaIliNerijeseno();
            }
        }

        private void pb11_mup(object sender, MouseEventArgs e)
        {
            if (!i.vazeciPotez(1, 1)) MessageBox.Show("To polje je već iskorišteno!", "Greška!");
            else
            {
                i.potez(1, 1);
                draw(g11);
                pobjedaIliNerijeseno();
            }
        }

        private void pb10_mup(object sender, MouseEventArgs e)
        {
            if (!i.vazeciPotez(1, 0)) MessageBox.Show("To polje je već iskorišteno!", "Greška!");
            else
            {
                i.potez(1, 0);
                draw(g10);
                pobjedaIliNerijeseno();
            }
        }

        private void pb02_mup(object sender, MouseEventArgs e)
        {
            if (!i.vazeciPotez(0, 2)) MessageBox.Show("To polje je već iskorišteno!", "Greška!");
            else
            {
                i.potez(0, 2);
                draw(g02);
                pobjedaIliNerijeseno();
            }
        }

        private void pb01_mup(object sender, MouseEventArgs e)
        {
            if (!i.vazeciPotez(0, 1)) MessageBox.Show("To polje je već iskorišteno!", "Greška!");
            else
            {
                i.potez(0, 1);
                draw(g01);
                pobjedaIliNerijeseno();
            }
        }

        private void pb00_mup(object sender, MouseEventArgs e)
        {
            if (!i.vazeciPotez(0, 0)) MessageBox.Show("To polje je već iskorišteno!", "Greška!");
            else
            {
                i.potez(0, 0);
                draw(g00);
                pobjedaIliNerijeseno();
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            
        }
    }
}
